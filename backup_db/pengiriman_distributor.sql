-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 02, 2018 at 01:13 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.0.31-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_distributor`
--

CREATE TABLE `pengiriman_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jumlah_pengiriman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman_distributor`
--

INSERT INTO `pengiriman_distributor` (`id`, `distributor_id`, `produk_id`, `jumlah_pengiriman`) VALUES
(50, 1, 1, 105),
(51, 1, 2, 83),
(52, 1, 3, 279),
(53, 1, 4, 91),
(54, 1, 5, 311),
(55, 1, 6, 54);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengiriman_distributor`
--
ALTER TABLE `pengiriman_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`distributor_id`),
  ADD KEY `itemID` (`produk_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pengiriman_distributor`
--
ALTER TABLE `pengiriman_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pengiriman_distributor`
--
ALTER TABLE `pengiriman_distributor`
  ADD CONSTRAINT `pengiriman_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengiriman_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
