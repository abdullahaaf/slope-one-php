-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 31, 2018 at 07:45 AM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.0.32-2+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `area` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`area_id`, `area`) VALUES
(1, 'SURABAYA'),
(2, 'SIDOARJO'),
(3, 'MALANG'),
(4, 'MOJOKERTO'),
(5, 'JOMBANG'),
(6, 'TRENGGALEK'),
(7, 'BANGKALAN'),
(8, 'JEMBER'),
(9, 'LUMAJANG'),
(10, 'PROBOLINGGO'),
(11, 'SITUBONDO');

-- --------------------------------------------------------

--
-- Table structure for table `area_prediction_result`
--

CREATE TABLE `area_prediction_result` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_prediction_result`
--

INSERT INTO `area_prediction_result` (`id`, `area_id`, `produk_id`, `nilai_rating`) VALUES
(5, 3, 1, '1.16'),
(6, 10, 2, '1.06'),
(7, 7, 3, '4.65'),
(8, 8, 4, '2.61'),
(9, 5, 5, '2.32'),
(10, 9, 6, '2.62');

-- --------------------------------------------------------

--
-- Table structure for table `dev_rating_area`
--

CREATE TABLE `dev_rating_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `unrated_produk_id` int(11) NOT NULL,
  `rated_produk_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dev_rating_area`
--

INSERT INTO `dev_rating_area` (`id`, `area_id`, `unrated_produk_id`, `rated_produk_id`, `result`) VALUES
(36, 3, 1, 2, '0.22'),
(37, 3, 1, 3, '-1.50'),
(38, 3, 1, 4, '0.00'),
(39, 3, 1, 5, '-0.50'),
(40, 3, 1, 6, '-0.30'),
(41, 10, 2, 1, '-0.12'),
(42, 10, 2, 3, '-1.60'),
(43, 10, 2, 4, '-0.10'),
(44, 10, 2, 5, '-0.50'),
(45, 10, 2, 6, '-0.40'),
(46, 7, 3, 1, '1.68'),
(47, 7, 3, 2, '1.59'),
(48, 7, 3, 4, '1.56'),
(49, 7, 3, 5, '1.11'),
(50, 7, 3, 6, '1.33'),
(51, 8, 4, 1, '0.08'),
(52, 8, 4, 2, '0.09'),
(53, 8, 4, 3, '-1.56'),
(54, 8, 4, 5, '-0.44'),
(55, 8, 4, 6, '-0.11'),
(56, 5, 5, 1, '0.48'),
(57, 5, 5, 2, '0.49'),
(58, 5, 5, 3, '-1.06'),
(59, 5, 5, 4, '0.44'),
(60, 5, 5, 6, '0.22'),
(61, 9, 6, 1, '0.08'),
(62, 9, 6, 2, '0.49'),
(63, 9, 6, 3, '-1.36'),
(64, 9, 6, 4, '0.14'),
(65, 9, 6, 5, '-0.23');

-- --------------------------------------------------------

--
-- Table structure for table `dev_rating_distributor`
--

CREATE TABLE `dev_rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `unrated_produk_id` int(11) NOT NULL,
  `rated_produk_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dev_rating_distributor`
--

INSERT INTO `dev_rating_distributor` (`id`, `distributor_id`, `unrated_produk_id`, `rated_produk_id`, `result`) VALUES
(1, 3, 1, 2, '0.60'),
(2, 3, 1, 3, '-1.40'),
(3, 3, 1, 4, '0.40'),
(4, 3, 1, 5, '-0.60'),
(5, 3, 1, 6, '1.40'),
(6, 1, 2, 1, '-0.41'),
(7, 1, 2, 3, '-1.80'),
(8, 1, 2, 4, '-0.20'),
(9, 1, 2, 5, '-1.00'),
(10, 1, 2, 6, '1.60'),
(11, 4, 3, 1, '1.25'),
(12, 4, 3, 2, '1.70'),
(13, 4, 3, 4, '1.20'),
(14, 4, 3, 5, '0.40'),
(15, 4, 3, 6, '2.80'),
(16, 13, 4, 1, '-0.25'),
(17, 13, 4, 2, '0.20'),
(18, 13, 4, 3, '-1.15'),
(19, 13, 4, 5, '-0.60'),
(20, 13, 4, 6, '2.00'),
(21, 10, 5, 1, '0.59'),
(22, 10, 5, 2, '1.03'),
(23, 10, 5, 3, '-0.48'),
(24, 10, 5, 4, '0.67'),
(25, 10, 5, 6, '2.60'),
(26, 8, 6, 1, '-1.58'),
(27, 8, 6, 2, '-1.47'),
(28, 8, 6, 3, '-2.98'),
(29, 8, 6, 4, '-1.83'),
(30, 8, 6, 5, '-2.51');

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `distributor_id` int(11) NOT NULL,
  `nama_distributor` varchar(23) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `distributor`
--

INSERT INTO `distributor` (`distributor_id`, `nama_distributor`, `region`) VALUES
(1, ' BP NIDHOM SURABAYA ', 'sidoarjo'),
(2, ' BP MUHAMMAD ', 'sidoarjo'),
(3, ' BP ANDI TROPODO ', 'sidoarjo'),
(4, ' BP NOVI SURABAYA ', 'sidoarjo'),
(5, ' IBU SOFI ', 'sidoarjo'),
(6, ' BP FAUZAN SEPANJANG ', 'sidoarjo'),
(7, ' BP NANANG NURSYAH ', 'sidoarjo'),
(8, ' H MAKSUM ', 'sidoarjo'),
(9, ' IBU NONY ', 'sidoarjo'),
(10, ' DA\'I MART ', 'sidoarjo'),
(11, ' BP Totok ', 'sidoarjo'),
(12, ' MAS THOMY SIDOARJO ', 'sidoarjo'),
(13, ' BP SAIKHUL HUDA ', 'sidoarjo'),
(14, ' GUS ALI ', 'sidoarjo'),
(15, ' IBU LILIS / BP NODHOM ', 'sidoarjo'),
(16, ' IBU ARDIANA ', 'sidoarjo'),
(17, ' SABILILLAH MART ', 'sidoarjo'),
(18, ' BP AINUDDIN ', 'sidoarjo'),
(19, 'UST AFIF', 'BANGKALAN');

-- --------------------------------------------------------

--
-- Table structure for table `distributor_prediction_result`
--

CREATE TABLE `distributor_prediction_result` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributor_prediction_result`
--

INSERT INTO `distributor_prediction_result` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
(1, 3, 1, '3.48'),
(2, 1, 2, '1.83'),
(3, 4, 3, '4.91'),
(4, 13, 4, '1.99'),
(5, 10, 5, '3.06'),
(6, 8, 6, '0.93');

-- --------------------------------------------------------

--
-- Table structure for table `not_rated_area`
--

CREATE TABLE `not_rated_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `ratingValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `not_rated_distributor`
--

CREATE TABLE `not_rated_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `ratingValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `jenis_produk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `nama_produk`, `jenis_produk`) VALUES
(1, '19', 'galon'),
(2, '120', 'gelas'),
(3, '240', 'gelas'),
(4, '330', 'botol'),
(5, '600', 'botol'),
(6, '1500', 'botol');

-- --------------------------------------------------------

--
-- Table structure for table `rating_area`
--

CREATE TABLE `rating_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_area`
--

INSERT INTO `rating_area` (`id`, `area_id`, `produk_id`, `nilai_rating`) VALUES
(51, 1, 1, '4'),
(52, 1, 2, '3'),
(53, 1, 3, '4'),
(54, 1, 4, '4'),
(55, 1, 5, '4'),
(56, 1, 6, '4'),
(63, 2, 1, '4'),
(64, 2, 2, '4'),
(65, 2, 3, '4'),
(66, 2, 4, '4'),
(67, 2, 5, '4'),
(68, 2, 6, '3'),
(69, 4, 1, '1'),
(70, 4, 2, '1'),
(71, 4, 3, '3'),
(72, 4, 4, '1'),
(73, 4, 5, '1'),
(74, 4, 6, '2'),
(75, 5, 1, '2'),
(76, 5, 2, '1'),
(77, 5, 3, '4'),
(78, 5, 4, '2'),
(80, 5, 6, '2'),
(81, 6, 1, '1'),
(82, 6, 2, '1'),
(83, 6, 3, '4'),
(84, 6, 4, '1'),
(85, 6, 5, '1'),
(86, 6, 6, '1'),
(87, 7, 1, '4'),
(88, 7, 2, '2'),
(90, 7, 4, '3'),
(91, 7, 5, '4'),
(92, 7, 6, '3'),
(93, 8, 1, '3'),
(94, 8, 2, '2'),
(95, 8, 3, '4'),
(97, 8, 5, '3'),
(98, 8, 6, '3'),
(99, 9, 1, '1'),
(100, 9, 2, '4'),
(101, 9, 3, '4'),
(102, 9, 4, '2'),
(103, 9, 5, '3'),
(105, 3, 2, '2'),
(106, 3, 3, '3'),
(107, 3, 4, '1'),
(108, 3, 5, '1'),
(109, 3, 6, '1'),
(111, 11, 3, '3'),
(112, 11, 4, '2'),
(113, 11, 5, '3'),
(114, 11, 6, '3'),
(115, 10, 1, '1'),
(116, 10, 3, '3'),
(117, 10, 4, '1'),
(118, 10, 5, '2'),
(120, 11, 2, '1'),
(121, 10, 6, '1'),
(122, 11, 1, '1'),
(124, 3, 1, '1.16'),
(125, 10, 2, '1.06'),
(126, 7, 3, '4.65'),
(127, 8, 4, '2.61'),
(128, 5, 5, '2.32'),
(129, 9, 6, '2.62');

-- --------------------------------------------------------

--
-- Table structure for table `rating_distributor`
--

CREATE TABLE `rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_distributor`
--

INSERT INTO `rating_distributor` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
(1, 1, 1, '2'),
(3, 1, 4, '2'),
(4, 1, 5, '3'),
(5, 1, 6, '1'),
(13, 3, 2, '4'),
(14, 3, 3, '4'),
(15, 3, 4, '4'),
(16, 3, 5, '4'),
(17, 3, 6, '1'),
(18, 4, 1, '4'),
(19, 4, 2, '4'),
(21, 4, 4, '4'),
(22, 4, 5, '4'),
(23, 4, 6, '1'),
(24, 8, 1, '4'),
(25, 8, 2, '2'),
(26, 8, 3, '4'),
(27, 8, 4, '2'),
(28, 8, 5, '3'),
(29, 10, 1, '2'),
(30, 10, 2, '2'),
(31, 10, 3, '4'),
(32, 10, 4, '2'),
(34, 10, 6, '1'),
(35, 13, 1, '1'),
(36, 13, 2, '1'),
(37, 13, 3, '4'),
(39, 13, 5, '3'),
(40, 13, 6, '1'),
(41, 15, 1, '3'),
(42, 15, 2, '2'),
(43, 15, 3, '4'),
(44, 15, 4, '3'),
(45, 15, 5, '4'),
(46, 15, 6, '1'),
(48, 1, 3, '3'),
(51, 3, 1, '3.48'),
(52, 1, 2, '1.83'),
(53, 4, 3, '4.91'),
(54, 13, 4, '1.99'),
(55, 10, 5, '3.06'),
(56, 8, 6, '0.93');

-- --------------------------------------------------------

--
-- Table structure for table `real_rating_area`
--

CREATE TABLE `real_rating_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_rating_area`
--

INSERT INTO `real_rating_area` (`id`, `area_id`, `produk_id`, `nilai_rating`) VALUES
(51, 1, 1, '4'),
(52, 1, 2, '3'),
(53, 1, 3, '4'),
(54, 1, 4, '4'),
(55, 1, 5, '4'),
(56, 1, 6, '4'),
(63, 2, 1, '4'),
(64, 2, 2, '4'),
(65, 2, 3, '4'),
(66, 2, 4, '4'),
(67, 2, 5, '4'),
(68, 2, 6, '3'),
(69, 4, 1, '1'),
(70, 4, 2, '1'),
(71, 4, 3, '3'),
(72, 4, 4, '1'),
(73, 4, 5, '1'),
(74, 4, 6, '2'),
(75, 5, 1, '2'),
(76, 5, 2, '1'),
(77, 5, 3, '4'),
(78, 5, 4, '2'),
(79, 5, 5, '2'),
(80, 5, 6, '2'),
(81, 6, 1, '1'),
(82, 6, 2, '1'),
(83, 6, 3, '4'),
(84, 6, 4, '1'),
(85, 6, 5, '1'),
(86, 6, 6, '1'),
(87, 7, 1, '4'),
(88, 7, 2, '2'),
(89, 7, 3, '4'),
(90, 7, 4, '3'),
(91, 7, 5, '4'),
(92, 7, 6, '3'),
(93, 8, 1, '3'),
(94, 8, 2, '2'),
(95, 8, 3, '4'),
(96, 8, 4, '2'),
(97, 8, 5, '3'),
(98, 8, 6, '3'),
(99, 9, 1, '1'),
(100, 9, 2, '4'),
(101, 9, 3, '4'),
(102, 9, 4, '2'),
(103, 9, 5, '3'),
(104, 9, 6, '3'),
(105, 3, 2, '2'),
(106, 3, 3, '3'),
(107, 3, 4, '1'),
(108, 3, 5, '1'),
(109, 3, 6, '1'),
(110, 11, 2, '1'),
(111, 11, 3, '3'),
(112, 11, 4, '2'),
(113, 11, 5, '3'),
(114, 11, 6, '3'),
(115, 10, 1, '1'),
(116, 10, 3, '3'),
(117, 10, 4, '1'),
(118, 10, 5, '2'),
(119, 7, 3, '4'),
(120, 8, 4, '2'),
(121, 5, 5, '2'),
(122, 9, 6, '3'),
(123, 3, 1, '0'),
(124, 10, 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `real_rating_distributor`
--

CREATE TABLE `real_rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_rating_distributor`
--

INSERT INTO `real_rating_distributor` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
(1, 1, 1, '2'),
(2, 1, 2, '1'),
(3, 1, 4, '2'),
(4, 1, 5, '3'),
(5, 1, 6, '1'),
(13, 3, 2, '4'),
(14, 3, 3, '4'),
(15, 3, 4, '4'),
(16, 3, 5, '4'),
(17, 3, 6, '1'),
(18, 4, 1, '4'),
(19, 4, 2, '4'),
(20, 4, 3, '4'),
(21, 4, 4, '4'),
(22, 4, 5, '4'),
(23, 4, 6, '1'),
(24, 8, 1, '4'),
(25, 8, 2, '2'),
(26, 8, 3, '4'),
(27, 8, 4, '2'),
(28, 8, 5, '3'),
(29, 10, 1, '2'),
(30, 10, 2, '2'),
(31, 10, 3, '4'),
(32, 10, 4, '2'),
(33, 10, 5, '3'),
(34, 10, 6, '1'),
(35, 13, 1, '1'),
(36, 13, 2, '1'),
(37, 13, 3, '4'),
(38, 13, 4, '1'),
(39, 13, 5, '3'),
(40, 13, 6, '1'),
(41, 15, 1, '3'),
(42, 15, 2, '2'),
(43, 15, 3, '4'),
(44, 15, 4, '3'),
(45, 15, 5, '4'),
(46, 15, 6, '1'),
(47, 8, 6, '3'),
(48, 1, 3, '3'),
(50, 3, 1, '3');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view rated produk 19`
-- (See below for the actual view)
--
CREATE TABLE `view rated produk 19` (
`area` varchar(255)
,`nama_produk` varchar(255)
,`nilai_rating` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `view rated produk 19`
--
DROP TABLE IF EXISTS `view rated produk 19`;

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadmin`@`localhost` SQL SECURITY DEFINER VIEW `view rated produk 19`  AS  select `area`.`area` AS `area`,`produk`.`nama_produk` AS `nama_produk`,`rating_area`.`nilai_rating` AS `nilai_rating` from ((`rating_area` join `area` on((`area`.`area_id` = `rating_area`.`area_id`))) join `produk` on((`produk`.`produk_id` = `rating_area`.`produk_id`))) where (`rating_area`.`produk_id` = 1) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `unrated_produk_id` (`unrated_produk_id`),
  ADD KEY `rated_produk_id` (`rated_produk_id`);

--
-- Indexes for table `dev_rating_distributor`
--
ALTER TABLE `dev_rating_distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`distributor_id`);

--
-- Indexes for table `distributor_prediction_result`
--
ALTER TABLE `distributor_prediction_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`distributor_id`),
  ADD KEY `itemID` (`produk_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `rating_area`
--
ALTER TABLE `rating_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wilayah_id` (`area_id`),
  ADD KEY `item_id` (`produk_id`);

--
-- Indexes for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`distributor_id`),
  ADD KEY `itemID` (`produk_id`);

--
-- Indexes for table `real_rating_area`
--
ALTER TABLE `real_rating_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `real_rating_distributor`
--
ALTER TABLE `real_rating_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `distributor_id` (`distributor_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `dev_rating_distributor`
--
ALTER TABLE `dev_rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `distributor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `distributor_prediction_result`
--
ALTER TABLE `distributor_prediction_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `rating_area`
--
ALTER TABLE `rating_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `real_rating_area`
--
ALTER TABLE `real_rating_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `real_rating_distributor`
--
ALTER TABLE `real_rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  ADD CONSTRAINT `area_prediction_result_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `area_prediction_result_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  ADD CONSTRAINT `dev_rating_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dev_rating_area_ibfk_2` FOREIGN KEY (`unrated_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dev_rating_area_ibfk_3` FOREIGN KEY (`rated_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  ADD CONSTRAINT `not_rated_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `not_rated_area_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  ADD CONSTRAINT `not_rated_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `not_rated_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating_area`
--
ALTER TABLE `rating_area`
  ADD CONSTRAINT `rating_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_area_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  ADD CONSTRAINT `rating_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `real_rating_area`
--
ALTER TABLE `real_rating_area`
  ADD CONSTRAINT `real_rating_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `real_rating_area_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `real_rating_distributor`
--
ALTER TABLE `real_rating_distributor`
  ADD CONSTRAINT `real_rating_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `real_rating_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
