<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 30/07/18
 * Time: 14:22
 */

class Dashboard extends CI_Controller
{
    public function index()
    {
        $this->data['bunch_of_distributor'] = $this->Mdistributor->getAllDistributor();
        $this->data['bunch_of_produk']      = $this->Mproduk->getAllProduk();
        $this->data['sites']                = "Dashboard";
        $this->data['pages']                = "Dashboard";
        $this->data['content']              = $this->load->view('dashboard', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function storeDistributor()
    {
        $nama_distributor  = $this->input->post('nama_distributor');
        $region             = $this->input->post('region');
    }
}