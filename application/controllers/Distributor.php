<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['bunch_of_distributor']                 = $this->Mdistributor->getAllDistributor();
        $this->data['bunch_of_produk']                      = $this->Mproduk->getAllProduk();
        $this->data['bunch_of_rating_produk']               = $this->Mdistributor->getRatingProduk();
        $this->data['sites']                                = "Data Rating distributor";
        $this->data['pages']                                = "Data Rating distributor";
        $this->data['content']                              = $this->load->view('distributor/view_rating_produk',$this->data,true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexDeviasi()
    {
        $this->data['bunch_of_produk']          = $this->Mdistributor->getRatedProduk();
        $this->data['bunch_of_unrated_produk']  = $this->Mdistributor->getUnratedProduk();
        $this->data['bunch_of_nilai_deviasi']   = $this->Mdistributor->getDeviation();
        $this->data['sites']                    = "Proses Perhitungan Nilai Deviasi";
        $this->data['pages']                    = "Proses perhitungan Nilai Deviasi";
        $this->data['content']                  = $this->load->view('distributor/view_deviasi', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexPrediksi()
    {
        $this->data['bunch_of_hasil_prediksi']  = $this->Mdistributor->getPredictionResult();
        $this->data['bunch_of_unrated_produk']  = $this->Mdistributor->getUnratedProduk();
        $this->data['sites']                    = "Proses Perhitungan Prediksi Nilai Rating";
        $this->data['pages']                    = "Proses perhitungan Prediksi Nilai Rating";
        $this->data['content']                  = $this->load->view('distributor/view_prediksi_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexLogRating()
    {
        $this->data['pages']                    = "Log Rating Distributor";
        $this->data['sites']                    = "Log Rating Distributor";
        $this->data['bunch_of_rating_produk']   = $this->Mdistributor->getPredictionResult();
        $this->data['content']                  = $this->load->view('distributor/view_log_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexChartRatingPerProduk()
    {
        $this->data['sites'] = "Statistik rating untuk setiap produk di setiap distributor";
        $this->data['pages'] = "Statistik Data";
        $this->data['produk19'] = $this->Mdistributor->getStatsOfProduk19();
        $this->data['produk120'] = $this->Mdistributor->getStatsOfProduk120();
        $this->data['produk240'] = $this->Mdistributor->getStatsOfProduk240();
        $this->data['produk330'] = $this->Mdistributor->getStatsOfProduk330();
        $this->data['produk600'] = $this->Mdistributor->getStatsOfProduk600();
        $this->data['produk1500'] = $this->Mdistributor->getStatsOfProduk1500();
        $this->data['content'] = $this->load->view('distributor/view_chart_rating_per_produk', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexRmse()
    {

         $this->data['sites']               = "Mean Absolute Error (MAE)";
         $this->data['pages']               = "Pengukuran Akurasi dengan MAE";
         $this->data['content']             = $this->load->view('distributor/view_rmse', $this->data, true);
         $this->load->view('layouts/layout', $this->data);

    }

    public function storeRatingProduk()
    {
        $distributor        = $this->input->post('distributor');
        $produk             = $this->input->post('produk');
        $jumlah_pengiriman  = $this->input->post('jumlah_pengiriman');

        $cek_rating = $this->Mdistributor->checkExistRating($distributor,$produk);
        if ($cek_rating > 0) {
            redirect('Distributor');
        } else {
            if ($jumlah_pengiriman > 0){
                if ($jumlah_pengiriman > 0 and $jumlah_pengiriman <= 83){
                    $nilai_rating = 1;
                }elseif ($jumlah_pengiriman > 83 and $jumlah_pengiriman <=185){
                    $nilai_rating = 2;
                }elseif ($jumlah_pengiriman > 185 and $jumlah_pengiriman <= 419){
                    $nilai_rating = 3;
                }elseif ($jumlah_pengiriman > 419){
                    $nilai_rating = 4;
                }
                $this->Mdistributor->storeRatingProduk($distributor,$produk,$nilai_rating);
            }else{
                $this->Mdistributor->storeUnratedProduk($distributor,$produk,$jumlah_pengiriman);
            }
            redirect(base_url('Distributor'));
        }
    }

    public function storeDeviationResult($distributor_id,$unrated_produk_id,$rated_produk_id,$result)
    {
        $check_deviasi  = $this->Mdistributor->checkExistDeviation($distributor_id,$unrated_produk_id,$rated_produk_id);
        if ($check_deviasi > 0) {
            redirect(base_url('Distributor/indexDeviasi'));
        }else {
            $this->Mdistributor->storeDeviationResult($distributor_id,$unrated_produk_id,$rated_produk_id,$result);
        }
    }

    public function storePredictionResult($distributor_id,$produk_id,$nilai_rating)
    {
        $check_rating   = $this->Mdistributor->checkExistPredictedRating($distributor_id,$produk_id);
        if ($check_rating > 0) {
            redirect(base_url('Distributor/indexPrediksi'));
        }else{
            $this->Mdistributor->storePredictionResult($distributor_id,$produk_id,$nilai_rating);
            $this->Mdistributor->storeRatingProduk($distributor_id,$produk_id,$nilai_rating);
            $this->Mdistributor->deleteUnratedProduk($distributor_id);
        }
    }

    /**
     * barisan kode dibawah ini merupakan implementasi
     * algoritma slope one
     */

    public function countDeviation($unrated_produk,$rated_produk)
    {
        $a = 0;
        $rating_from_target_produk      = $this->Mdistributor->getRatingFromTargetProduk($unrated_produk,$rated_produk);
        $rating_from_compared_produk    = $this->Mdistributor->getRatingFromComparedProduk($unrated_produk,$rated_produk);
        $distributor_target             = $this->Mdistributor->getUnratedDistributor($unrated_produk);

        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang akan
         * diprediksi menjadi satu array
         */
        foreach ($rating_from_target_produk as $value) {
            $rating_value_target_produk[] = $value->nilai_rating;
        }

        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang
         * akan dibandingkan menjadi
         * satu array
         */
        foreach ($rating_from_compared_produk as $value) {
            $rating_value_compared_produk[] = $value->nilai_rating;
        }

        /**
         * kode dibawah ini digunakan untuk menjadikan
         * hasil pengurangan nilai rating produk yang
         * akan diprediksi nilai rating nya dengan produk
         * yang akan dibandingkan menjadi satu array
         */
        foreach ($rating_value_target_produk as $key => $value) {
            $substract_result[]   = $value - $rating_value_compared_produk[$key];
        }

        /**
         * kode dibawah ini digunakan untuk
         * menjumlahkan hasil dari pengurangan
         * diatas
         */
        foreach ($substract_result as  $value) {
            $a  = $a+=$value;
        }

        $number_of_distributor          = count($rating_value_compared_produk);
        $deviation_result               = $a/$number_of_distributor;
        
        $deviation_array = array(
            'distributor_id'    => $distributor_target->distributor_id,
            'unrated_produk_id' => $unrated_produk,
            'rated_produk_id'   => $rated_produk,
            'nilai_deviasi'     => $deviation_result
        );
        echo json_encode($deviation_array);
    }

    public function countPrediction($distributor_id)
    {
        $nilai_atas = 0;
        $nilai_bawah = 0;

        $unrated_produk         = $this->Mdistributor->getUnratedProdukByDistributor($distributor_id);
        foreach ($unrated_produk as $produk){
            $unrated_produk_id  = $produk->produk_id;
        }

        /*
         * kode ini digunakan untuk menjadikan rating dari distributor
         * target menjadi single array
         */
        $user_rating    = $this->Mdistributor->getUserRating($distributor_id);
        foreach ($user_rating as $rating) {
            $rating_value_of_distributor_target[] = $rating->nilai_rating;
        }

        /*
         * kode ini digunakan untuk menjadikan nilai deviasi produk
         * yang akan diprediksi nilai rating nya dengan produk yang
         * telah dirating oleh distributor target menjadi single array
         */
        $user_deviation = $this->Mdistributor->getUserDeviation($distributor_id);
        foreach ($user_deviation as $deviasi) {
            $deviation_result[]           = $deviasi->result;
            $rated_produk_in_deviation[]  = $deviasi->rated_produk_id;
        }

        foreach($rated_produk_in_deviation as $key => $value) {
            $number_of_distributor_rated_both_produk[] =   $this->Mdistributor->countRatedProduk($unrated_produk_id,$value);
        }
        $number_of_produk_rated_by_distributor_target   = $this->Mdistributor->countRatedProdukByDistributorTarget($distributor_id);
        for($i = 0 ; $i < $number_of_produk_rated_by_distributor_target ; $i++) {
            $prediksi[] = ($deviation_result[$i] + $rating_value_of_distributor_target[$i]) * $number_of_distributor_rated_both_produk[$i];
        }

        foreach ($prediksi as $value) {
            $nilai_atas = $nilai_atas += $value;
        }

        foreach ($number_of_distributor_rated_both_produk as $value) {
            $nilai_bawah = $nilai_bawah += $value;
        }

        $hasil_prediksi     = $nilai_atas/$nilai_bawah;

        /*
         * kode ini digunakan untuk membuat array dari
         * data hasil prediksi nilai rating produk
         */
        $prediction_result  = array(
            'distributor_id'    => $distributor_id,
            'produk_id'         => $unrated_produk_id,
            'nilai_rating'      => number_format($hasil_prediksi,2)
        );
        echo json_encode($prediction_result);

    }
}
