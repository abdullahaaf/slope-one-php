<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Area extends CI_Controller
{
	
	public function index()
    {
        $this->data['sites']                        = 'Data Rating Area';
        $this->data['pages']                        = 'Data Rating Area';
        $this->data['bunch_of_area']                = $this->Marea->getArea();
        $this->data['bunch_of_produk']              = $this->Mproduk->getAllProduk();
        $this->data['bunch_of_rating_produk']    	= $this->Marea->getRatingProduk();
        $this->data['content']                      = $this->load->view('area/view_rating_produk', $this->data,true);
        $this->load->view('layouts/layout',$this->data);
    }

    public function indexDeviasi()
    {
        $this->data['bunch_of_produk']          = $this->Marea->getRatedProduk();
        $this->data['bunch_of_unrated_produk']  = $this->Marea->getUnratedProduk();
        $this->data['bunch_of_nilai_deviasi']   = $this->Marea->getDeviation();
        $this->data['sites']                    = "Proses Perhitungan Nilai Deviasi";
        $this->data['pages']                    = "Proses perhitungan Nilai Deviasi";
        $this->data['content']                  = $this->load->view('area/view_deviasi', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexPrediksi()
    {
        $this->data['bunch_of_unrated_produk']  = $this->Marea->getUnratedProduk();
        $this->data['bunch_of_hasil_prediksi']  = $this->Marea->getPredictionResult();
        $this->data['sites']                    = "Proses Perhitungan Prediksi Nilai Rating";
        $this->data['pages']                    = "Proses perhitungan Prediksi Nilai Rating";
        $this->data['content']                  = $this->load->view('area/view_prediksi_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexLogRating()
    {
        $this->data['bunch_of_rating_produk']   = $this->Marea->getPredictionResult();
        $this->data['pages']                    = "Log Rating Area";
        $this->data['sites']                    = "Log Rating Area";
        $this->data['content']                  = $this->load->view('area/view_log_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexChartRatingPerProduk()
    {
        $this->data['sites'] = "Statistik rating untuk setiap produk di area distribusi";
        $this->data['pages'] = "Statistik Data";
        $this->data['produk19']   = $this->Marea->getStatsOfProduk19();
        $this->data['produk120']  = $this->Marea->getStatsOfProduk120();
        $this->data['produk240']  = $this->Marea->getStatsOfProduk240();
        $this->data['produk330']  = $this->Marea->getStatsOfProduk330();
        $this->data['produk600']  = $this->Marea->getStatsOfProduk600();
        $this->data['produk1500']  = $this->Marea->getStatsOfProduk1500();
        $this->data['content'] = $this->load->view('area/view_chart_rating_per_produk', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexChartRatingPerArea()
    {
        $this->data['sites'] = "Statistik rating semua produk di setiap area";
        $this->data['pages'] = "Statistik Data";
        $this->data['area_surabaya']   = $this->Marea->getStatsOfAreaSurabaya();
        $this->data['area_sidoarjo']   = $this->Marea->getStatsOfAreaSidoarjo();
        $this->data['area_malang']   = $this->Marea->getStatsOfAreaMalang();
        $this->data['area_mojokerto']   = $this->Marea->getStatsOfAreaMojokerto();
        $this->data['area_jombang']   = $this->Marea->getStatsOfAreaJombang();
        $this->data['area_trenggalek']   = $this->Marea->getStatsOfAreaTrenggalek();
        $this->data['area_bangkalan']   = $this->Marea->getStatsOfAreaBangkalan();
        $this->data['area_jember']   = $this->Marea->getStatsOfAreaJember();
        $this->data['area_lumajang']   = $this->Marea->getStatsOfAreaLumajang();
        $this->data['area_probolinggo']   = $this->Marea->getStatsOfAreaProbolinggo();
        $this->data['area_situbondo']   = $this->Marea->getStatsOfAreaSitubondo();
        $this->data['content'] = $this->load->view('area/view_chart_rating_per_area', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function countDeviation($unrated_produk,$rated_produk)
    {
        $a = 0;
        $rating_from_target_produk  = $this->Marea->getRatingFromTargetproduk($unrated_produk,$rated_produk);
        $rating_from_compared_produk    = $this->Marea->getRatingFromComparedProduk($unrated_produk,$rated_produk);
        $area_target    = $this->Marea->getUnratedArea($unrated_produk);

        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang akan
         * diprediksi menjadi satu array
         */
        foreach ($rating_from_target_produk as $value) {
            $rating_value_target_produk[] = $value->nilai_rating;
        }
        // echo json_encode($rating_value_target_produk);

        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang
         * akan dibandingkan menjadi
         * satu array
         */
        foreach ($rating_from_compared_produk as $value) {
            $rating_value_compared_produk[] = $value->nilai_rating;
        }

        // echo json_encode($rating_value_target_produk);
        // echo "<br>";
        // echo json_encode($rating_value_compared_produk);

        /**
         * kode dibawah ini digunakan untuk menjadikan
         * hasil pengurangan nilai rating produk yang
         * akan diprediksi nilai rating nya dengan produk
         * yang akan dibandingkan menjadi satu array
         */
        foreach ($rating_value_target_produk as $key => $value) {
            $substract_result[]   = $value - $rating_value_compared_produk[$key];
        }

        /**
         * kode dibawah ini digunakan untuk
         * menjumlahkan hasil dari pengurangan
         * diatas
         */
        foreach ($substract_result as  $value) {
            $a  = $a+=$value;
        }

        $number_of_distributor          = count($rating_value_compared_produk);
        $deviation_result               = $a/$number_of_distributor;

        $array_result   = array(
            'unrated_produk_id' => $unrated_produk,
            'rated_produk_id'   => $rated_produk,
            'nilai_deviasi'     => $deviation_result,
            'area_id'           => $area_target->area_id
        );
        echo json_encode($array_result);
    }

    public function countPrediction($area_id)
    {
        $nilai_atas     = 0;
        $nilai_bawah    = 0;

        $unrated_produk         = $this->Marea->getUnratedProdukByArea($area_id);
        foreach ($unrated_produk as $produk){
            $unrated_produk_id  = $produk->produk_id;
        }

        /*
         * kode ini digunakan untuk menjadikan rating dari area
         * target menjadi single array
         */
        $area_rating    = $this->Marea->getAreaRating($area_id);
        foreach ($area_rating as $rating) {
            $rating_value_of_area_target[] = $rating->nilai_rating;
        }

        /*
         * kode ini digunakan untuk menjadikan nilai deviasi produk
         * yang akan diprediksi nilai rating nya dengan produk yang
         * telah dirating oleh area target menjadi single array
         */
        $area_deviation = $this->Marea->getAreaDeviation($area_id);
        foreach ($area_deviation as $deviasi) {
            $deviation_result[]           = $deviasi->result;
            $rated_produk_in_deviation[]  = $deviasi->rated_produk_id;
        }

        foreach($rated_produk_in_deviation as $key => $value) {
            $number_of_area_rated_both_produk[] =   $this->Marea->countRatedProduk($unrated_produk_id,$value);
        }
        $number_of_produk_rated_by_area_target   = $this->Marea->countRatedProdukByAreaTarget($area_id);

        for($i = 0 ; $i < $number_of_produk_rated_by_area_target ; $i++) {
            $prediksi[] = ($deviation_result[$i] + $rating_value_of_area_target[$i]) * $number_of_area_rated_both_produk[$i];
        }

        foreach ($prediksi as $value) {
            $nilai_atas = $nilai_atas += $value;
        }

        foreach ($number_of_area_rated_both_produk as $value) {
            $nilai_bawah = $nilai_bawah += $value;
        }

        $hasil_prediksi     = $nilai_atas/$nilai_bawah;

        /*
         * kode ini digunakan untuk membuat array dari
         * data hasil prediksi nilai rating produk
         */
        $prediction_result  = array(
            'area_id'           => $area_id,
            'produk_id'         => $unrated_produk_id,
            'nilai_rating'      => $hasil_prediksi
        );
        echo json_encode($prediction_result);
    }

    public function storeDeviationResult($area_id, $unrated_produk_id, $rated_produk_id, $result)
    {
        $check_deviasi  = $this->Marea->checkExistDeviasi($area_id,$unrated_produk_id,$area_id,$rated_produk_id);
        if ($check_deviasi > 0) {

        }else {
            $this->Marea->storeDeviationResult($area_id,$unrated_produk_id,$rated_produk_id,$result);
        }
    }

    public function storePredictionResult($area_id, $produk_id, $nilai_rating)
    {
        $check_prediksi_rating  = $this->Marea->checkExistPredictedRating($area_id,$produk_id);
        if ($check_prediksi_rating > 0) {
            redirect(base_url('Area/indexPrediksi'));
            
        }else {
            $this->Marea->storePredictionResult($area_id,$produk_id,$nilai_rating);
            $this->Marea->storeRatingProduk($area_id,$produk_id,$nilai_rating);
            $this->Marea->deleteUnratedProduk($area_id,$produk_id);
        }
    }

    public function storeRatingProduk()
    {
        $area               = $this->input->post('area');
        $produk             = $this->input->post('produk');
        $jumlah_pengiriman  = $this->input->post('jumlah_pengiriman');

        $check_rating   = $this->Marea->checkExistRating($area,$produk);
        if ($check_rating > 0) {
            redirect(base_url('Area'));
        }else {
            if ($jumlah_pengiriman > 0) {
                if ($jumlah_pengiriman > 0 and $jumlah_pengiriman <=165) {
                    $rating_value   = 1;
                }elseif ($jumlah_pengiriman > 165 and $jumlah_pengiriman <= 485) {
                    $rating_value   = 2;
                }elseif ($jumlah_pengiriman > 485   and $jumlah_pengiriman <= 1540) {
                    $rating_value   = 3;
                }elseif ($jumlah_pengiriman > 1540) {
                    $rating_value   = 4;
                }
                $this->Marea->storeRatingProduk($area,$produk,$rating_value);
            }else {
                $this->Marea->storeNotRated($area,$produk,'0');
            }
        }
        redirect(base_url('Area'));
    }

    public function MAE()
    {
        $a = array();
        $observed_rating    = $this->Marea->getPredictionResult();
        foreach ($observed_rating as $value) {
            $observed_rating_value[]    = (float)$value->nilai_rating;
            $area_id[]    = $value->area_id;
            $produk_id[]  = $value->produk_id;
        }
        
        $actual_rating      = $this->Marea->getRealRating($area_id,$produk_id);
        foreach ($actual_rating as $value) {
            $actual_rating_value[] = $value->nilai_rating;
        }
        echo json_encode($actual_rating_value);
    }
}
?>