<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Surabaya</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_surabaya" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Sidoarjo</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_sidoarjo" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Malang</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_malang" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Mojokerto</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_mojokerto" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Jombang</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_jombang" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di Area Trenggalek</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_trenggalek" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Bangkalan</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_bangkalan" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di Area Jember</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_jember" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Lumajang</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_lumajang" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di Area Probolinggo</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_probolinggo" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title"><strong>Statistik rating distribusi produk di area Situbondo</strong></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="area_situbondo" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>

<?php foreach ($area_surabaya as  $value) {
    $rating_area_surabaya[] = $value->nilai_rating;
}?>

<?php foreach ($area_sidoarjo as  $value) {
    $rating_area_sidoarjo[] = $value->nilai_rating;
}?>

<?php foreach ($area_malang as  $value) {
    $rating_area_malang[] = $value->nilai_rating;
}?>

<?php foreach ($area_mojokerto as  $value) {
    $rating_area_mojokerto[] = $value->nilai_rating;
}?>

<?php foreach ($area_jombang as  $value) {
    $rating_area_jombang[] = $value->nilai_rating;
}?>

<?php foreach ($area_trenggalek as  $value) {
    $rating_area_trenggalek[] = $value->nilai_rating;
}?>

<?php foreach ($area_bangkalan as  $value) {
    $rating_area_bangkalan[] = $value->nilai_rating;
}?>

<?php foreach ($area_jember as  $value) {
    $rating_area_jember[] = $value->nilai_rating;
}?>

<?php foreach ($area_lumajang as  $value) {
    $rating_area_lumajang[] = $value->nilai_rating;
}?>

<?php foreach ($area_probolinggo as  $value) {
    $rating_area_probolinggo[] = $value->nilai_rating;
}?>

<?php foreach ($area_situbondo as  $value) {
    $rating_area_situbondo[] = $value->nilai_rating;
}?>


<script>
var area_surabaya = document.getElementById("area_surabaya").getContext('2d');
var area_sidoarjo = document.getElementById("area_sidoarjo").getContext('2d');
var area_malang = document.getElementById("area_malang").getContext('2d');
var area_mojokerto = document.getElementById("area_mojokerto").getContext('2d');
var area_jombang = document.getElementById("area_jombang").getContext('2d');
var area_trenggalek = document.getElementById("area_trenggalek").getContext('2d');
var area_bangkalan = document.getElementById("area_bangkalan").getContext('2d');
var area_jember = document.getElementById("area_jember").getContext('2d');
var area_lumajang = document.getElementById("area_lumajang").getContext('2d');
var area_probolinggo = document.getElementById("area_probolinggo").getContext('2d');
var area_situbondo = document.getElementById("area_situbondo").getContext('2d');

var options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Nilai Rating'
                }
            }],
             xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Jenis Produk'
                }
            }]
        },
    };
var labels = ['19','120','240','330','600','1500'];

Chart.defaults.global.defaultFontColor = 'black';
Chart.defaults.global.defaultFontStyle = 'bold';
var area_surabaya = new Chart(area_surabaya, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_surabaya)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_sidoarjo = new Chart(area_sidoarjo, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_sidoarjo)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_malang = new Chart(area_malang, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_malang)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_mojokerto = new Chart(area_mojokerto, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_mojokerto)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_jombang = new Chart(area_jombang, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_jombang)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_trenggalek = new Chart(area_trenggalek, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_trenggalek)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});
var area_bangkalan = new Chart(area_bangkalan, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_bangkalan)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_jember = new Chart(area_jember, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_jember)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_lumajang = new Chart(area_lumajang, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_lumajang)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_probolinggo = new Chart(area_probolinggo, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_probolinggo)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var area_situbondo = new Chart(area_situbondo, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_area_situbondo)?>,
            backgroundColor: '#2196f3',
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});
</script>
</div>