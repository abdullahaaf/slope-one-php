<div class="col-md-12">
    <div class="box pad box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="col-md-10">
                            <select class="form-control" style="width: 100%; font-weight: bold;" id="unrated_produk" >
                                <?php foreach ($bunch_of_unrated_produk as $produk) { ?>
                                    <option value="<?php echo $produk->area_id?>"><?php echo $produk->area?> - <?php echo $produk->nama_produk?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Proses">
                            <button type="button" id="btnPrediksi" class="btn btn-flat btn-primary pull-left" data-toggle="modal"
                                    data-target="#modal-prediksi" onclick="countPrediction();" style="font-weight: bold;">
                                <span class="fa fa-calculator" area-hidden="true"></span> Hitung Prediksi
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box pad box-success">
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover dataTable data-posts">
                <thead>
                <tr>
                    <th width="1%"></th>
                    <th width="6%;">Area</th>
                    <th width="3%;">Nama Produk</th>
                    <th width="3%;">Nilai Prediksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach ($bunch_of_hasil_prediksi as $prediksi) { ?>
                    <tr>
                        <td align="center;" style="font-weight: bold;">
                            <?php
                            echo $no;
                            $no++;
                            ?>
                        </td>
                        <td align="center;" style="font-weight: bold;"><?php echo $prediksi->area?></td>
                        <td align="center;" style="font-weight: bold;"><?php echo $prediksi->nama_produk?></td>
                        <td align="center;" style="font-weight: bold;"><?php echo $prediksi->nilai_rating?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-prediksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="text-align: center; font-weight: bold;">Prediksi Nilai Rating</h3>
                </div>
                <div class="modal-body">
                    <p class="lead" style="text-align: center; font-weight: bold;" id="nilai-rating">test</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" onclick="location.reload();" style="font-weight: bold;">Kembali</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

    <script type="text/javascript">
        function countPrediction()
        {
            var unrated_produk  = $("#unrated_produk").val();
            $.ajax({
                url     : "<?php echo base_url('Area/countPrediction')?>/"+unrated_produk,
                type    : "GET",
                success : function (data)
                {
                    var mydata  = $.parseJSON(data);
                    var area_id         = mydata.area_id;
                    var produk_id       = mydata.produk_id;
                    var nilai_rating    = mydata.nilai_rating.toFixed(2);

                    var html    = "<p class='lead' style='text-align: center; font-weight: bold;'>"+nilai_rating+"</p>";
                    document.getElementById("nilai-rating").innerHTML = html;

                    $.ajax({
                        url: "<?php echo base_url('Area/storePredictionResult')?>/"+area_id+"/"+produk_id+"/"+nilai_rating,
                        type: "POST"
                    });
                }
            });
        }
    </script>
</div>