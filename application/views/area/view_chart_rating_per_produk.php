<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 19 L</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart19" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 120 ml</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart120" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 240 ml</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart240" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 330 ml</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart330" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 600 ml</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart600" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Statistik rating produk 1500 ml</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>
                <div class="box-body">
                <div class="chart">
                    <canvas id="chart1500" style="height:250px"></canvas>
                </div>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
    </div>

<?php foreach ($produk19 as  $value) {
    $area_19[] = $value->area;
    $rating_19[] = $value->nilai_rating;
}?>

<?php foreach ($produk120 as $value) {
    $rating_120[] = $value->nilai_rating;
}?>

<?php foreach ($produk240 as $value) {
    $rating_240[] = $value->nilai_rating;
}?>

<?php foreach ($produk330 as $value) {
    $rating_330[] = $value->nilai_rating;
}?>

<?php foreach ($produk600 as $value) {
    $rating_600[] = $value->nilai_rating;
}?>

<?php foreach ($produk1500 as $value) {
    $rating_1500[] = $value->nilai_rating;
}?>

<script>
var chart19 = document.getElementById("chart19").getContext('2d');
var chart120 = document.getElementById("chart120").getContext('2d');
var chart240 = document.getElementById("chart240").getContext('2d');
var chart330 = document.getElementById("chart330").getContext('2d');
var chart600 = document.getElementById("chart600").getContext('2d');
var chart1500 = document.getElementById("chart1500").getContext('2d');
var options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
    };
var labels = ['Bangkalan','Jember','Jombang','Lumajang','Malang','Mojokerto','Probolinggo','Sidoarjo','Situbondo','Surabaya','Trenggalek'];
    // var area = '<?php echo json_encode($area_19)?>';
    // var labels = JSON.parse(area);
var chart19 = new Chart(chart19, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_19)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var chart120 = new Chart(chart120, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_120)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var chart1240 = new Chart(chart240, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_240)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var chart330 = new Chart(chart330, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_330)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var chart600 = new Chart(chart600, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_600)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});

var chart1500 = new Chart(chart1500, {
    type: 'line',
    data: {
        labels: labels ,
        datasets: [{
            label: 'Nilai Rating',
            data: <?php echo json_encode($rating_1500)?>,
            borderColor: 'black',
            borderWidth: 3,
            fill: false,
        }]
    },
    options: options,
});
</script>
</div>