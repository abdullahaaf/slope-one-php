<div class="col-md-12">
    <div class="box pad box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-weight: bold;">Pilih produk</label>
                        <select class="form-control" style="width: 100%; font-weight: bold;" id="unrated_produk">
                            <?php foreach ($bunch_of_unrated_produk as $data_rating) { ?>
                                <option value="<?php echo $data_rating->produk_id?>"><?php echo $data_rating->nama_distributor?> - <?php echo $data_rating->nama_produk?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-weight: bold;">Pilih produk yang akan di bandingkan</label>
                        <select class="form-control" style="width: 100%; font-weight: bold;" id="rated_produk">
                            <?php foreach ( $bunch_of_produk as $produk) { ?>
                                <option value="<?php echo $produk->produk_id?>"><?php echo $produk->nama_produk?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Proses">
                    <button type="button" class="btn btn-flat btn-primary center-block" data-toggle="modal"
                            data-target="#modal-deviasi" onclick="countDeviation()" style="font-weight: bold;">
                        <span class="fa fa-calculator" area-hidden="true"></span> Hitung
                    </button>
                </a>
            </div>
        </div>
    </div>
    <div class="box pad box-success">
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover dataTable data-posts">
                <thead>
                <tr>
                    <th width="1%"></th>
                    <th width="6%;">Nama Distributor</th>
                    <th width="3%;">Produk 1</th>
                    <th width="3%;">Produk 2</th>
                    <th width="3%;">Nilai Deviasi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach ( $bunch_of_nilai_deviasi as $nilai_deviasi) { ?>
                    <tr>
                        <td align="center;" style="font-weight: bold;">
                            <?php
                            echo $no;
                            $no++;
                            ?>
                        </td>
                        <td align="center;" style="font-weight: bold;"><?php echo $nilai_deviasi->nama_distributor?></td>
                        <td align="center;" style="font-weight: bold;"><?php echo $nilai_deviasi->produk_1?></td>
                        <td align="center;" style="font-weight: bold;"><?php echo $nilai_deviasi->produk_2?></td>
                        <td align="center;" style="font-weight: bold;"><?php echo $nilai_deviasi->result?></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-deviasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="text-align: center; font-weight: bold;">Niali Deviasi</h3>
                </div>
                <div class="modal-body">
                    <p class="lead" style="text-align: center; font-weight: bold;" id="nilai-deviasi">test</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" onclick="location.reload();" style="font-weight: bold;">Kembali</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        /*

         */
        function countDeviation()
        {
            var unrated_produk  = $('#unrated_produk').val();
            var rated_produk    = $('#rated_produk').val();
            $.ajax({
                url         : "<?php echo base_url('Distributor/countDeviation')?>/"+unrated_produk+"/"+rated_produk,
                type        : "GET",
                success     : function (data) {
                    var mydata          = $.parseJSON(data);
                    var nilai_deviasi   = mydata.nilai_deviasi.toFixed(2);
                    var distributor_id  = mydata.distributor_id;
                    var unrated_produk  = mydata.unrated_produk_id;
                    var rated_produk    = mydata.rated_produk_id;

                    var html    = "<p class='lead' style='text-align: center; font-weight: bold;'>"+nilai_deviasi+"</p>";
                    document.getElementById("nilai-deviasi").innerHTML = html;

                    $.ajax({
                        url:    "<?php echo base_url('Distributor/storeDeviationResult')?>/"+distributor_id+"/"+unrated_produk+"/"+rated_produk+"/"+nilai_deviasi,
                        type:   "POST"
                    });
                }
            });
        }
    </script>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

</div>