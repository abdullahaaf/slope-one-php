<div class="col-md-12">
    <!--    modal tambah data rating-->
    <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="<?php echo base_url('Distributor/storeRatingProduk')?>" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">Tambah Data Rating Baru</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table-striped table-hover table">
                            <div class="form-group">
                                <label style="font-weight: bold;">Nama Distributor</label>
                                <select class="form-control" style="width: 100%; font-weight: bold;" name="distributor">
                                    <?php foreach ( $bunch_of_distributor as $distributor) { ?>
                                        <option value="<?php echo $distributor->distributor_id?>"><strong><?php echo $distributor->nama_distributor?></strong></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label style="font-weight: bold;">Jenis Produk</label>
                                <select class="form-control" style="width: 100%; font-weight: bold;" name="produk">
                                    <?php foreach ( $bunch_of_produk as $produk) { ?>
                                        <option value="<?php echo $produk->produk_id?>"><strong><?php echo $produk->nama_produk?></strong></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label style="font-weight: bold;">Jumlah Pengiriman</label>
                                <input type="text" name="jumlah_pengiriman" class="form-control" style="width: 100%;">
                            </div>
                        </table>
                    </div>
                    <div class="modal-footer" style="margin-right: 15px;">
                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal" style="font-weight: bold;">Kembali
                        </button>
                        <input type="submit" id ="simpan" class="btn btn-flat btn-primary" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--    end modal tambah data rating-->
    <div class="row" style="padding-right: 16px;">
        <div class="pull-right" style="margin-right: 0px;">
            <button type="button" class="btn btn-flat btn-warning fixed-button" data-toggle="modal" data-target="#modal-tambah" id="fixedbutton">
                <span class="glyphicon glyphicon-plus" area-hidden="true"></span>
                <span class="hidden-xs">Tambah Data</span>
            </button>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div class="box pad box-success">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover dataTable data-posts">
                    <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="6%;">Nama Distributor</th>
                        <th width="3%;">Jenis Produk</th>
                        <th width="3%;">Nilai Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ( $bunch_of_rating_produk as $data_rating) { ?>
                        <tr>
                            <td align="center;" style="font-weight: bold;">
                                <?php
                                echo $no;
                                $no++;
                                ?>
                            </td>
                            <td align="center;" style="font-weight: bold;"><?php echo $data_rating->nama_distributor?></td>
                            <td align="center;" style="font-weight: bold;"><?php echo $data_rating->nama_produk?></td>
                            <td align="center;" style="font-weight: bold;"><?php echo $data_rating->nilai_rating?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

</div>
