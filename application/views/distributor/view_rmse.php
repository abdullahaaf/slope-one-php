<div class="col-md-12">
    <div class="callout callout-warning">
        <h1>Mean Absolute Error (MAE)</h1>
        <p class="lead" style="">Nilai hasil dari MAE merepresentasikan rata – rata kesalahan (error) absolut antara hasil prediksi/peramalan dengan nilai sebenarnya</p>
    </div>
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-lime">
                <div class="inner">
                    <h3>0,064</h3>
                        <p>Nilai MAE Rating Area</p>
                </div>
                <div class="icon">
                    <i class="fa fa-tags"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-fuchsia-active">
                <div class="inner">
                    <h3>0,127</h3>
                        <p>Nilai MAE Rating Distributor</p>
                </div>
                <div class="icon">
                    <i class="fa fa-tags"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-lime">
                <div class="inner">
                    <h3>99.93</h3>
                        <p>Persentase Akurasi Prediksi Rating Area</p>
                </div>
                <div class="icon">
                    <i class="fa fa-percent"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-fuchsia-active">
                <div class="inner">
                    <h3>99.87</h3>
                        <p>Persentase Akurasi Prediksi Rating Distributor</p>
                </div>
                <div class="icon">
                    <i class="fa fa-percent"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
    </div>
</div>