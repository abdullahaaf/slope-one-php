<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SKRIPSI PROJECT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css');?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/morris.js/morris.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/jvectormap/jquery-jvectormap.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css');?>">
     <!-- Google Font -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/font.css');?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/select2/dist/css/select2.min.css')?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');?>">

    <!-- jQuery 3 -->
    <script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo base_url('assets/bower_components/jquery-ui/jquery-ui.min.js');?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>s -->
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <!-- Morris.js charts -->
    <script src="<?php echo base_url('assets/bower_components/raphael/raphael.min.js');?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url('assets/bower_components/jquery-knob/dist/jquery.knob.min.js');?>"></script>
    <!-- daterangepicker -->
    <script src="<?php echo base_url('assets/bower_components/moment/min/moment.min.js');?>"></script>
    <script src="<?php echo base_url('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/bower_components/fastclick/lib/fastclick.js');?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/adminlte.min.js');?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url('assets/dist/js/pages/dashboard.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js');?>"></script>

    <!-- chartjs -->
    <script type="text/javascript" src="<?php echo base_url('assets/chartjs/Chart.bundle.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/chartjs/Chart.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/chartjs/Chart.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/chartjs/Chart.min.js')?>"></script>
    <!-- end chart js -->

</head>
<body class="hold-transition skin-green-light sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="<?php echo base_url('Dashboard')?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>S</b>LP</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>SlopeOne</b></span>
            </a>
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">

                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="treeview">
                        <a href="#">
                            <i class="glyphicon glyphicon-floppy-saved"></i>
                            <span>Input</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('Distributor')?>"><i class="fa fa-users"></i> <span>Input rating distributor</span></a></li>
                            <li><a href="<?php echo base_url('Area')?>"><i class="fa fa-map"></i> <span>Input pengiriman area</span></a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="glyphicon glyphicon-transfer"></i>
                            <span>Proses</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('Distributor/indexDeviasi')?>"><i class="fa fa-calculator"></i> <span>Hitung Deviasi Distributor</span></a></li>
                            <li><a href="<?php echo base_url('Area/indexDeviasi')?>"><i class="fa fa-calculator"></i> <span>Hitung Deviasi Area</span></a></li>
                            <li><a href="<?php echo base_url('Distributor/indexPrediksi')?>"><i class="fa fa-lightbulb-o"></i> <span>Prediksi Rating Distributor</span></a></li>
                            <li><a href="<?php echo base_url('Area/indexPrediksi')?>"><i class="fa fa-lightbulb-o"></i> <span>Prediksi Rating Area</span></a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="glyphicon glyphicon-paste"></i>
                            <span>Output</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('Area/indexChartRatingPerProduk')?>"><i class="fa fa-circle-o"></i> <span>Statistik Rating Per Produk<br>(Area)</span></a></li>
                            <li><a href="<?php echo base_url('Distributor/indexChartRatingPerProduk')?>"><i class="fa fa-circle-o"></i> <span>Statistik Rating Per Produk<br>(Distributor)</span></a></li>
                            <li><a href="<?php echo base_url('Area/indexChartRatingPerArea')?>"><i class="fa fa-circle-o"></i> <span>Statistik Rating Per Area</span></a></li>

                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-history"></i>
                            <span>Log Rating</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('Distributor/indexLogRating')?>"><i class="fa fa-circle-o"></i> <span>Log Rating Distributor</span></a></li>
                            <li><a href="<?php echo base_url('Area/indexLogRating')?>"><i class="fa fa-circle-o"></i> <span>Log Rating Area</span></a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('Distributor/indexRmse')?>"><i class="fa fa-check-circle"></i> <span>Mean Absolute Error<br>of Slope One</span></a></li>
                    <!-- <li><a href="#"><i class="fa fa-bitbucket"></i> <span>Kesimpulan</span></a></li> -->
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?php echo $sites?>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url('Dashboard')?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><?php echo $pages?></li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <?php echo $content ?>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> beta
            </div>
            <strong>Created by <a href="#">Abdullah Amin Firdaus</a> for <a href="#">Undergraduate Thesis Project</a></strong>
        </footer>
    </div>
</body>
</html>
