<!--    modal tambah distributor-->
<div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo base_url('Dashboard/storeDistributor')?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="exampleModalLabel">Tambah Data Distributor</h3>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-hover">
                        <div class="form-group">
                            <label>Nama Distributor</label>
                            <input type="text" class="form-control" style="width: 100%;" name="user_name">
                        </div>
                        <div class="form-group">
                            <label>Area</label>
                            <input type="text" class="form-control" style="width: 100%;" name="region">
                        </div>
                    </table>
                </div>
                <div class="modal-footer" style="margin-right: 15px;">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Kembali
                    </button>
                    <input type="submit" id ="simpan" class="btn btn-flat btn-success" value="Simpan">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="box pad box-success">
                <div class="box-header">
                    <div class="pull-left" style="margin-right: 0px;">
                        <p class="lead">Daftar Distributor</p>
                    </div>
                    <div class="pull-right" style="margin-left: 0px;">
                        <button type="button" class="btn btn-flat btn-warning fixed-button" data-toggle="modal" data-target="#modal-tambah" id="fixedbutton">
                            <span class="glyphicon glyphicon-plus" area-hidden="true"></span>
                            <span class="hidden-xs">Tambah Distributor</span>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dataTable" id="data-posts">
                        <thead>
                        <tr>
                            <th width="2%;"></th>
                            <th width="6%;">Nama Distributor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ( $bunch_of_distributor as $distributor) { ?>
                            <tr>
                                <td align="center;">
                                    <?php
                                    echo $no;
                                    $no++;
                                    ?>
                                </td>
                                <td align="center;"><?php echo $distributor->nama_distributor?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box pad box-success">
                <div class="box-header">
                    <div class="pull-left" style="margin-right: 0px;">
                        <p class="lead">Daftar Produk</p>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dataTable" id="data-posts">
                        <thead>
                        <tr>
                            <th width="2%;"></th>
                            <th width="5%;">Nama Produk</th>
                            <th width="5%;">Jenis Produk</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ( $bunch_of_produk as $produk) { ?>
                            <tr>
                                <td align="center;">
                                    <?php
                                    echo $no;
                                    $no++;
                                    ?>
                                </td>
                                <td align="center;"><?php echo $produk->nama_produk?></td>
                                <td align="center;"><?php echo $produk->jenis_produk?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <script type="text/javascript" class="init" language="javascript">
            $(function () {
                $('#data-posts').DataTable();
            });
        </script>

    </div>
</div>
