<?php
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 28/09/18
 * Time: 15:50
 */

class Marea extends CI_Model
{
	/*
	* fungsi ini digunakan untuk menampilkan
	* seluruh area
	*/
    public function getArea()
    {
        $query  = $this->db->get('area');
        return $query->result();
    }

    /**
     * fungsi ini digunaka untuk menampilkan
     * area yang belum merating produk dan
     * telah dihitung nilai deviasi nya
     */
    public function getAreaInNotRatedTable()
    {
        $sql    = "SELECT * FROM area WHERE area_id IN(SELECT area_id FROM not_rated_area) ";
        $sql    .= "AND area_id IN (SELECT area_id FROM dev_rating_area)";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /**
     * fungsi ini digunakan untuk menampilkan
     * produk yang belum dirating dan telah dihitung
     * nilai deviasi nya
     */
    public function getProdukInNotRatedTable()
    {
        $sql    = "SELECT not_rated_area.produk_id, produk.nama_produk, produk.jenis_produk FROM not_rated_area
        JOIN produk ON produk.produk_id = not_rated_area.produk_id
        WHERE not_rated_area.produk_id IN (SELECT dev_rating_area.unrated_produk_id FROM dev_rating_area)
        AND not_rated_area.area_id IN (SELECT dev_rating_area.area_id FROM dev_rating_area)";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
	* fungsi ini digunakan untuk menampilkan produk
	* yang belum dirating oleh area target
    */
    function getUnratedProduk()
    {
    	$this->db->join('area', 'area.area_id = not_rated_area.area_id');
    	$this->db->join('produk','produk.produk_id = not_rated_area.produk_id');
        $this->db->order_by('area', 'ASC');
        $this->db->select('not_rated_area.area_id, area.area,produk.nama_produk, not_rated_area.produk_id');
        $query = $this->db->get('not_rated_area');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data nilai
     * deviasi setiap produk air minum
     */
    public function getDeviation()
    {
        $sql = "SELECT area.area, p1.nama_produk as produk_1, p2.nama_produk AS produk_2, dev.result
                FROM dev_rating_area AS dev
                JOIN area ON area.area_id = dev.area_id
                JOIN produk AS p1 ON p1.produk_id = dev.unrated_produk_id
                JOIN produk AS p2 on p2.produk_id = dev.rated_produk_id ORDER BY p2.produk_id ASC ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
    * fungsi ini digunakan untuk menampilkan
    * produk yang sudah dirating
    */
    function getRatingProduk()
    {
        $this->db->join('area', 'rating_area.area_id = area.area_id');
        $this->db->join('produk', 'rating_area.produk_id = produk.produk_id');
        $this->db->order_by('area','ASC');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk mendapatkan nilai rating
     * produk-produk yang telah dirating oleh area target
     */
    function getAreaRating($area_id)
    {
        $this->db->where('area_id',$area_id);
        $this->db->order_by('rating_area.produk_id','ASC');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data
     * hasil prediksi
     */
    function getPredictionResult()
    {
        $this->db->join('area','area.area_id = area_prediction_result.area_id');
        $this->db->join('produk','produk.produk_id = area_prediction_result.produk_id');
        $this->db->order_by('area_prediction_result.area_id','ASC');
        $query  = $this->db->get('area_prediction_result');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan area
     * yang belum memberi nilai rating pada produk
     */
    public function getUnratedArea($produk_id)
    {
        $this->db->where('produk_id', $produk_id);
        $this->db->select('area_id');
        $query  = $this->db->get('not_rated_area');
        return $query->row();
    }

    /*
     * fungsi ini digunakan untuk mendapatkan nilai deviasi
     * dari produk yang akan diprediksi nilai rating nya
     * dengan produk-produk yang telah dirating
     * oleh area target
     */
    function getAreaDeviation($area_id)
    {
        $this->db->where('area_id', $area_id);
        $this->db->order_by('rated_produk_id', 'ASC');
        $query  = $this->db->get('dev_rating_area');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan produk
     * yang belum dirating oleh area target
     */
    public function getUnratedProdukByArea($area_id)
    {
        $sql    = "SELECT produk_id FROM not_rated_area WHERE area_id =".$area_id;
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan hanya
     * produk yang sudah di rating
     */
    public function getRatedProduk()
    {
        $sql = "SELECT produk.produk_id, produk.nama_produk FROM rating_area";
        $sql .= " JOIN produk ON produk.produk_id = rating_area.produk_id";
        $sql .= " WHERE rating_area.area_id = ";
        $sql .= " (SELECT not_rated_area.area_id FROM not_rated_area)";
        $sql .= " ORDER BY rating_area.produk_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan diprediksi
     * nilai rating nya
     */
    public function getRatingFromTargetProduk($unrated_produk,$rated_produk)
    {
        $sql    =  "SELECT area_id,nilai_rating FROM rating_area WHERE area_id ";
        $sql    .= "IN (SELECT area_id FROM rating_area WHERE produk_id = {$rated_produk} )"; 
        $sql    .= "AND produk_id = $unrated_produk ORDER BY area_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan dibandingkan
     * dengan produk target
     */
    public function getRatingFromComparedProduk($unrated_produk,$rated_produk)
    {
        $sql    = "SELECT * FROM rating_area WHERE area_id IN ";
        $sql    .= "(SELECT area_id FROM rating_area WHERE produk_id = {$unrated_produk}) ";
        $sql    .= "AND produk_id = {$rated_produk} ORDER BY area_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menyimpan nilai deviasi
     * kedalam tabel deviasi
     */
    function storeDeviationResult($area_id, $unrated_produk, $rated_produk, $result)
    {
        $data = array(
            'area_id'           => $area_id,
            'unrated_produk_id' => $unrated_produk,
            'rated_produk_id'   => $rated_produk,
            'result'            => $result
        );
        $this->db->insert('dev_rating_area', $data);
    }

    /*
     * fungsi ini digunkan untuk menyimpan
     * hasil prediksi rating
     */
    function storePredictionResult($area_id, $produk_id, $nilai_rating)
    {
        $data   = array(
            'area_id'       => $area_id,
            'produk_id'     => $produk_id,
            'nilai_rating'  => $nilai_rating
        );
        $this->db->insert('area_prediction_result', $data);
    }

    /*
     * fungsi ini digunakan untuk menambah
     * data rating baru
     */
    public function storeRatingProduk($area_id,$produk_id,$nilai_rating)
    {
        $data   = array(
            'area_id'       => $area_id,
            'produk_id'     => $produk_id,
            'nilai_rating'  => $nilai_rating
        );
        $this->db->insert('rating_area',$data);
    }

    /*
     * fungsi ini digunakan untuk menambah
     * data baru produk yang tidak
     * mendapat nilai rating
     */
    public function storeNotRated($area_id, $produk_id, $ratingValue)
    {
        $data   = array(
            'area_id'       => $area_id,
            'produk_id'     => $produk_id,
            'ratingValue'   => $ratingValue
        );
        $this->db->insert('not_rated_area', $data);
    }

    /*
     * fungsi dibawah ini digunakan untuk menghitung
     * jumlah area yang merating produk yang akan
     * di prediksi nilai rating nya dan produk
     * yang dibandingkan
     */
    function countAreaRatedBothProduk($unrated_produk)
    {
        $hasil = 0;
        $sql = "SELECT COUNT(*) AS jumlah_area FROM rating_area WHERE produk_id =".$unrated_produk;
        $query = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil = $q->jumlah_area;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk menghitung area
     * yang merating kedua produk,
     * produk yang akan di prediksi
     * dan produk yang dibandingkan
     */
    function countRatedProduk($unrated_produk, $rated_produk)
    {
        $hasil = 0;
        $sql    = "SELECT COUNT(*) as jumlah_area FROM rating_area WHERE area_id ";
        $sql    .= "IN (SELECT area_id FROM rating_area WHERE ";
        $sql    .= "produk_id = {$unrated_produk}) AND produk_id = $rated_produk";
        $query = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil = $q->jumlah_area;
        }
        return $hasil;
    }

    /**
     * fungsi ini digunakan untuk menampilkan
     * jumlah produk yang telah dirating
     * oleh distributor target
     */
    public function countRatedProdukByAreaTarget($area_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as jumlah_produk from rating_area ";
        $sql    .= "where area_id = {$area_id}";
        $query  = $this->db->query($sql);
        foreach($query->result() as $q) {
            $hasil =  $q->jumlah_produk;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa apakah
     * data rating yang di inputkan sudah ada
     * di database atau belum
     */
    public function checkExistRating($area_id,$produk_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as rating from rating_area where ";
        $sql    .= "area_id={$area_id} and produk_id={$produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa apakah
     * nilai deviasi yang di inputkan sudah
     * ada di database atau belum
     */
    public function checkExistDeviasi($area_id,$unrated_produk_id,$rated_produk_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as deviasi from dev_rating_area where ";
        $sql    .= "area_id={$area_id} and unrated_produk_id={$unrated_produk_id} ";
        $sql    .= "and rated_produk_id={$rated_produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result as $q) {
            $hasil  = $q->deviasi;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunkan untuk memeriksa apakah
     * data prediksi rating yang dimasukkan sudah ada
     * di database atau belum
     */
    public function checkExistPredictedRating($area_id,$produk_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as rating from area_prediction_result where ";
        $sql    .= "area_id={$area_id} and produk_id={$produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /**
    * fungsi ini digunakan untuk menghapus
    * data yang sudah diprediksi nilai rating nya
    */
    public function deleteUnratedProduk($area_id,$produk_id)
    {
        $this->db->where('area_id', $area_id);
        $this->db->where('produk_id', $produk_id);
        $this->db->delete('not_rated_area');
    }

    /**
     * kode dibawah ini untuk keperluan pembuatan
     * grafik
     */
    public function getStatsOfProduk19()
    {
        $this->db->where('rating_area.produk_id',1);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfProduk120()
    {
        $this->db->where('rating_area.produk_id',2);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfProduk240()
    {
        $this->db->where('rating_area.produk_id',3);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfProduk330()
    {
        $this->db->where('rating_area.produk_id',4);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfProduk600()
    {
        $this->db->where('rating_area.produk_id',5);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfProduk1500()
    {
        $this->db->where('rating_area.produk_id',6);
        $this->db->order_by('area.area','ASC');
        $this->db->join('area','area.area_id = rating_area.area_id');
        $this->db->select('area.area, rating_area.nilai_rating');
        $query  = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaSurabaya()
    {
        $this->db->where('rating_area.area_id', 1);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaSidoarjo()
    {
        $this->db->where('rating_area.area_id', 2);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaMalang()
    {
        $this->db->where('rating_area.area_id', 3);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaMojokerto()
    {
        $this->db->where('rating_area.area_id', 4);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaJombang()
    {
        $this->db->where('rating_area.area_id', 5);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaTrenggalek()
    {
        $this->db->where('rating_area.area_id', 6);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaBangkalan()
    {
        $this->db->where('rating_area.area_id', 7);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaJember()
    {
        $this->db->where('rating_area.area_id', 8);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaLumajang()
    {
        $this->db->where('rating_area.area_id', 9);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaProbolinggo()
    {
        $this->db->where('rating_area.area_id', 10);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    public function getStatsOfAreaSitubondo()
    {
        $this->db->where('rating_area.area_id', 11);
        $this->db->order_by('rating_area.produk_id','ASC');
        $this->db->join('produk','produk.produk_id = rating_area.produk_id');
        $this->db->select('produk.nama_produk, rating_area.nilai_rating');
        $query = $this->db->get('rating_area');
        return $query->result();
    }

    // kode dibawah ini untuk keperluan perhitungan Mean Absolute Error
    public function getRealRating($area_id,$produk_id)
    {
        $this->db->where('area_id', $area_id);
        $this->db->where('produk_id', $produk_id);
        $this->db->order_by('real_rating_area.produk_id',"ASC");
        $query  = $this->db->get('real_rating_area');
        return $query->result();
    }

}