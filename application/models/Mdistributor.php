<?php
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 27/07/18
 * Time: 17:53
 */

class Mdistributor extends CI_Model
{
    /*
     * fungsi ini digunakan untuk menampilkan
     * semua distributor
     */
    public function getAllDistributor()
    {
        $this->db->from('distributor');
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * fungsi ini digunakan untuk mendapatkan
     * data rating asli dari distributor
     */
    public function getRealRating()
    {
        $sql = "SELECT nilai_rating FROM real_rating_distributor ORDER by distributor_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    public function getPredictedRating()
    {
        $sql = "SELECT nilai_rating FROM rating_distributor ORDER by distributor_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan
     * semua data rating produk air minum
     * di setiap distributor
     */
    public function getRatingProduk()
    {
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->order_by('nama_distributor','ASC');
        $this->db->from('rating_distributor');
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan produk
     * yang belum mendapatkan nilai rating
     */
    public function getUnratedProduk()
    {
        $this->db->join('distributor','distributor.distributor_id = not_rated_distributor.distributor_id');
        $this->db->join('produk','produk.produk_id = not_rated_distributor.produk_id');
        $this->db->order_by('nama_distributor','ASC');
        $this->db->select('not_rated_distributor.distributor_id,distributor.nama_distributor,produk.nama_produk, not_rated_distributor.produk_id');
        $query = $this->db->get('not_rated_distributor');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan produk
     * yang belum dirating oleh user target
     */
    public function getUnratedProdukByDistributor($distributor_id)
    {
        $sql    = "SELECT produk_id FROM not_rated_distributor WHERE distributor_id =".$distributor_id;
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan hanya
     * produk yang sudah di rating
     */
    public function getRatedProduk()
    {
        $sql = "SELECT produk.produk_id, produk.nama_produk FROM rating_distributor";
        $sql .= " JOIN produk ON produk.produk_id = rating_distributor.produk_id";
        $sql .= " WHERE rating_distributor.distributor_id = ";
        $sql .= " (SELECT not_rated_distributor.distributor_id FROM not_rated_distributor)";
        $sql .= " ORDER BY rating_distributor.produk_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan distributor
     * yang belum memberi nilai rating pada produk
     */
    public function getUnratedDistributor($produk_id)
    {
        $this->db->where('produk_id', $produk_id);
        $this->db->select('distributor_id');
        $query  = $this->db->get('not_rated_distributor');
        return $query->row();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data nilai
     * deviasi setiap produk air minum
     */
    public function getDeviation()
    {
        $sql = "SELECT distributor.nama_distributor, p1.nama_produk as produk_1, p2.nama_produk AS produk_2, dev.result
FROM dev_rating_distributor AS dev
JOIN distributor ON distributor.distributor_id = dev.distributor_id
JOIN produk AS p1 ON p1.produk_id = dev.unrated_produk_id
JOIN produk AS p2 on p2.produk_id = dev.rated_produk_id ORDER BY p2.produk_id ASC ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data
     * hasil prediksi
     */
    function getPredictionResult()
    {
        $this->db->join('distributor','distributor.distributor_id = distributor_prediction_result.distributor_id');
        $this->db->join('produk','produk.produk_id = distributor_prediction_result.produk_id');
        $query  = $this->db->get('distributor_prediction_result');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menambahkan distributor baru
     */
    public function storeDistributor($nama_distributor, $region)
    {
        $data = array(
            'nama_distributor' => $nama_distributor,
            'region'    => $region
        );
        $this->db->insert('distributor', $data);
    }

    /*
     * fungsi ini digunakan untuk menambahkan rating
     * baru untuk setiap produk
     */
    public function storeRatingProduk($distributor_id, $produk_id, $nilai_rating)
    {
        $data = array(
            'distributor_id'        => $distributor_id,
            'produk_id'             => $produk_id,
            'nilai_rating'          => $nilai_rating,
        );
        $this->db->insert('rating_distributor', $data);
    }

    /*
     * fungsi ini digunakan untuk menambahkan produk
     * yang belum mendapatkan nilai rating
     */
    public function storeUnratedProduk($distributor_id, $produk_id, $nilai_rating)
    {
        $data = array(
            'distributor_id'        => $distributor_id,
            'produk_id'             => $produk_id,
            'ratingValue'           => $nilai_rating,
        );
        $this->db->insert('not_rated_distributor', $data);
    }

    /*
     * fungsi ini digunakan untuk menyimpan nilai deviasi
     * kedalam tabel deviasi
     */
    function storeDeviationResult($distributor_id, $unrated_produk, $rated_produk, $result)
    {
        $data = array(
            'distributor_id'    => $distributor_id,
            'unrated_produk_id' => $unrated_produk,
            'rated_produk_id'   => $rated_produk,
            'result'            => $result
        );
        $this->db->insert('dev_rating_distributor', $data);
    }

    /*
     * fungsi ini digunakan untuk menyimpan hasil prediksi
     * nilai rating
     */
    function storePredictionResult($distributor_id, $produk_id, $nilai_rating)
    {
        $data   = array(
            'distributor_id'    => $distributor_id,
            'produk_id'         => $produk_id,
            'nilai_rating'      => $nilai_rating
        );
        $this->db->insert('distributor_prediction_result', $data);
    }

    /**
     * fungsi ini digunakan untuk menyimpan hasil rating
     * ke tabel baru, untuk keperluan pengujian algoritma
     */
    public function storeRatingToNewTable($distributor_id,$produk_id,$nilai_rating)
    {
        $data   = array(
            'distributor_id'    => $distributor_id,
            'produk_id'         => $produk_id,
            'nilai_rating'      => $nilai_rating
        );
        $this->db->insert('new_rating_distributor', $data);
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan diprediksi
     * nilai rating nya
     */
    public function getRatingFromTargetProduk($unrated_produk,$rated_produk)
    {
        $sql    =  "SELECT distributor_id,nilai_rating FROM rating_distributor WHERE distributor_id ";
        $sql    .= "IN (SELECT distributor_id FROM rating_distributor WHERE produk_id = {$rated_produk} )"; 
        $sql    .= "AND produk_id = $unrated_produk ORDER BY distributor_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan dibandingkan
     * dengan produk target
     */
    public function getRatingFromComparedProduk($unrated_produk,$rated_produk)
    {
        $sql    = "SELECT * FROM rating_distributor WHERE distributor_id IN ";
        $sql    .= "(SELECT distributor_id FROM rating_distributor WHERE produk_id = {$unrated_produk}) ";
        $sql    .= "AND produk_id = {$rated_produk} ORDER BY distributor_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menghitung distributor
     * yang merating kedua produk
     * produk yang akan di prediksi
     * dan produk yang dibandingkan
     */
    function countRatedProduk($unrated_produk, $rated_produk)
    {
        $hasil = 0;
        $sql    = "SELECT COUNT(*) as jumlah_distributor FROM rating_distributor WHERE distributor_id ";
        $sql    .= "IN (SELECT distributor_id FROM rating_distributor WHERE ";
        $sql    .= "produk_id = {$unrated_produk}) AND produk_id = $rated_produk";
        $query = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil = $q->jumlah_distributor;
        }
        return $hasil;
    }

    /**
     * fungsi ini digunakan untuk menampilkan
     * jumlah produk yang telah dirating
     * oleh distributor target
     */
    public function countRatedProdukByDistributorTarget($distributor_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as jumlah_produk from rating_distributor ";
        $sql    .= "where distributor_id = {$distributor_id}";
        $query  = $this->db->query($sql);
        foreach($query->result() as $q) {
            $hasil =  $q->jumlah_produk;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk mendapatkan nilai rating
     * produk-produk yang telah dirating oleh user target
     */
    function getUserRating($distributor_id)
    {
        $this->db->where('distributor_id',$distributor_id);
        $this->db->order_by('produk_id','ASC');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }
    
    /*
     * fungsi ini digunakan untuk mendapatkan nilai deviasi
     * dari produk yang akan diprediksi nilai rating nya
     * dengan produk-produk yang telah dirating
     * oleh user target
     */
    function getUserDeviation($distributor_id)
    {
        $this->db->where('distributor_id', $distributor_id);
        $this->db->order_by('rated_produk_id', 'ASC');
        $query  = $this->db->get('dev_rating_distributor');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data rating yang dimasukkan
     * sudah ada didalam table atau belum
     */
    public function checkExistRating($distributor_id,$produk_id)
    {
        $hasil  = 0;
        $sql    = "SELECT COUNT(*) as rating from rating_distributor where ";
        $sql    .= "distributor_id={$distributor_id} and produk_id={$produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data deviasi sudah dimasukkan atau belum
     */
    public function checkExistDeviation($distributor_id,$unrated_produk_id,$rated_produk_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as deviasi from dev_rating_distributor where ";
        $sql    .= "distributor_id={$distributor_id} and unrated_produk_id={$unrated_produk_id} ";
        $sql    .= "and rated_produk_id={$rated_produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->deviasi;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data prediksi rating sudah dimasukkan atau belum
     */
    public function checkExistPredictedRating($distributor_id, $produk_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as rating from distributor_prediction_result where ";
        $sql    .= "distributor_id={$distributor_id} and produk_id={$produk_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /**
    * fungsi ini digunakan untuk menghapus
    * data yang sudah diprediksi nilai rating nya
    */
    public function deleteUnratedProduk($distributor_id)
    {
        $this->db->where('distributor_id', $distributor_id);
        $this->db->delete('not_rated_distributor');
    }

    // kode dibawah ini untuk keperluan menampilkan data dalam bentuk grafik
    public function getStatsOfProduk19()
    {
        $this->db->where('rating_distributor.produk_id',1);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }

    public function getStatsOfProduk120()
    {
        $this->db->where('rating_distributor.produk_id',2);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }

    public function getStatsOfProduk240()
    {
        $this->db->where('rating_distributor.produk_id',3);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }

    public function getStatsOfProduk330()
    {
        $this->db->where('rating_distributor.produk_id',4);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }

    public function getStatsOfProduk600()
    {
        $this->db->where('rating_distributor.produk_id',5);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }

    public function getStatsOfProduk1500()
    {
        $this->db->where('rating_distributor.produk_id',6);
        $this->db->join('produk','produk.produk_id = rating_distributor.produk_id');
        $this->db->join('distributor','distributor.distributor_id = rating_distributor.distributor_id');
        $this->db->order_by('rating_distributor.distributor_id','ASC');
        $this->db->select('distributor.nama_distributor,produk.nama_produk,rating_distributor.nilai_rating');
        $query  = $this->db->get('rating_distributor');
        return $query->result();
    }
}