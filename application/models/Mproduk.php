<?php
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 27/07/18
 * Time: 17:54
 */

class Mproduk extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProduk()
    {
        $this->db->from('produk');
        $query = $this->db->get();
        return $query->result();
    }
}